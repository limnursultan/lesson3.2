﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_001_Variable
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();

            int x = rand.Next(3);
            Console.WriteLine($"Получено значение Х = {x}");

            if (x == 1) Console.WriteLine("x == 1");
                else Console.WriteLine("х != 1");

            Console.ReadKey();
        }
    }
}
